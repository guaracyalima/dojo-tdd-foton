import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { ComissoesComponent } from './comissoes/comissoes.component';
import { FuncionariosComponent } from './funcionarios/funcionarios.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { VendedoresComponent } from './vendedores/vendedores.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'vendedores', component: VendedoresComponent },
  { path: 'comissoes', component: ComissoesComponent },
  { path: 'login', component: LoginComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
