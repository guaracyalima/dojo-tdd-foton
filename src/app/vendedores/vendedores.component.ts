import { Component, OnInit } from '@angular/core';
import { VendedoresService } from '../services/vendedores.service';

@Component({
  selector: 'app-vendedores',
  templateUrl: './vendedores.component.html',
  styleUrls: ['./vendedores.component.css']
})
export class VendedoresComponent implements OnInit {

  public vendedores: any;

  constructor(private _service: VendedoresService) { }

  ngOnInit(): void {
    //this.getAll();
    this.vendedores = [
      {
      nome: "Araujo",
      comissao: 15
      },
      {
      nome: "Jacomé",
      comissao: 5
      },
      {
      nome: "Tranca",
      comissao: 10
      },
      {
      nome: "Maciel",
      comissao: 3
      },
      {
      nome: "Silva",
      comissao: 1
      }
      ]
  }

  public getAll(): void {
    this._service.index().subscribe(data => this.vendedores = data, err => console.log('erro no get da parada', err))
  }

}
